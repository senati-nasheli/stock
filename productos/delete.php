<?php
require_once('../conexion.php');

$id=$_GET['id'];

$sql = "DELETE FROM productos WHERE id = :id";
$sth = $pdo->prepare($sql);
$sth->bindParam(':id', $id, PDO::PARAM_INT);
if( $sth->execute()){
    print "<script>alert('¡Producto eliminado con exito!');location='index.php';</script>";
}else{
    print "Error al eliminar del registro!<br><br>";
}
?>
